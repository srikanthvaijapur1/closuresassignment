function limitFunctionCallCount(cb, n){

    if (!n || !cb) return () => null;
  
    let counter = 0;
  
    const execute = (...parameter) => {
      if (counter < n) {
        counter++;
        return cb(...parameter);
      }
      return null;
    };
  
    return execute;
  };
  
  module.exports = limitFunctionCallCount;
  