function countFactory() {
    let counter = 0;
    const increment = (val = 1) => {
        return counter += val;
    }
    const decrement = (val = 1) => {
        return counter -= val
    }
    return {
        increment,
        decrement,
    };
};

module.exports = countFactory;