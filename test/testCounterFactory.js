const countFactory = require("../problems/counterFactory");

const result = countFactory();

console.log(result.increment());

console.log(result.increment(5));

console.log(result.decrement());

console.log(result.decrement(2));

